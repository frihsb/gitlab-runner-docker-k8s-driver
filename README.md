# Custom GitLab-Runner Executor: docker-k8s-driver

GitLab-Runners can have different executors. The most commonly used executor probably is the `docker` runner. This is great for standard use-cases like executing unit tests. However, for [rettij](https://gitlab.com/frihsb/rettij) integration tests, we need a kubernetes (or short: k8s) cluster to actually run the software and test if it behaves like expected. The results should obviously be reproducable and not differ due to e.g. old leftovers from previous runs. Ideally, that means having a k8s-cluster for each run that can be spinned up quickly and removed again after the integration tests were run. The [kind](https://kind.sigs.k8s.io/) software (which is short for "kubernetes in docker") offers exactly that.

## Motivation

In the past we actually made use of the existing VirtualBox-executor from the gitlab-runner but it turned out to be plagued by instability issues, because... well, VirtualBox. The machine needed frequent restarts and the VM itself had to be kept up-to-date as well. It was more work to keep the VirtualBox-runner working than we hoped for.

Thus, we decided to implement our own "Custom" executor, which is basically a gitlab-runner feature that allows you to plug in your own shell-scripts into the different stages. If you implement your own custom executor, that specifically is called a "driver", which is why this repository is called `docker-k8s-driver`. While rettij is run inside a docker container (providing propper segregation from the host), in parallel a separate k8s-cluster solely for that rettij instance is launched (inside more docker containers as handled by `kind`). The custom docker-k8s-driver handles all of this: It creates a kubeconfig that's automatically placed inside the main container (rettij in this case). It connects the main docker container to the same network as the k8s-cluster-containers and mounts the source code of the git-repository (that's cloned by gitlab-runner itself) into each of the containers (which is needed for some tests where rettij has the ability to mount files from the host system on which a k8s-node is run on).

Generally speaking, the implementation in this repository is very generic and not rettij-specific, so you might be able to easily adapt it to your needs as well. Or simply to setup your own gitlab-runner for your own rettij-repository-fork. This implementation is heavily based on the [LXD driver example](https://docs.gitlab.com/runner/executors/custom_examples/lxd.html) from the gitlab documentation.

## Installation

This installation guide assumes that your gitlab-runner host unsername is called `gitlab-runner`, otherwise please adapt the paths here and inside the scripts.

Place the following files inside `/opt/docker-k8s-driver`:
- `base.sh`: Defines some variables used inside other scripts
- `prepare.sh`: Prepares the environemtn for excuton (e.g. create the main docker container and kind-k8s-cluster)
- `run.sh`: Executes the scripts defined in the `.gitlab-ci.yaml`-file
- `cleanup.sh`: Cleans up the environment after execution (e.g. removes the docker container and kind-k8s-cluster)
- `config.sh`: Configures the build path and driver name
- `k8s-kind-cluster-config.yaml`: Configuration for kind-k8s-cluster
- `k8s-kind-cluster-setup.sh`: Setup kind-k8s-cluster with config mentioned above (cluster name and project directory to mount are specified as parameters when this script is run by `prepare.sh`)

Then there is the following file:
- `config.toml`: Register your own gitlab-runner on your gitlab-instance and then edit the respective runner-section accordingly to the content of this example file (you normally find your `config.toml` in `$HOME/gitlab-runner/config.toml` or `/root/gitlab-runner/config.toml` depending on how your gitlab-runner is set up (please refer to the official gitlab-documentation for for information)
