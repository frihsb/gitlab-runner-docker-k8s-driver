#!/usr/bin/env bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

set -eo pipefail

# trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

install_dependencies () {
    # Install Git LFS, git comes pre installed with ubuntu image.
    docker exec "$CONTAINER_ID" sh -c "apt-get update -y && apt-get install -y git-lfs"

    # Install gitlab-runner binary since we need for cache/artifacts.
    docker exec "$CONTAINER_ID" sh -c 'curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"'
    docker exec "$CONTAINER_ID" sh -c "chmod +x /usr/local/bin/gitlab-runner"
}

start_k8s_cluster () {
    # Create a k8s cluster with config and a unique name, the container ID. Also pass PROJECT_DIR so it can be mounted to the cluster nodes
    /opt/k8s-kind-cluster-setup.sh "$CONTAINER_ID" "$PROJECT_DIR"
    # Safe kubeconfig file for newly created cluster to file
    kind export kubeconfig --kubeconfig "$KUBECONF_NAME" --name $CONTAINER_ID --internal
}

start_container () {
    if docker container inspect "$CONTAINER_ID" >/dev/null 2>/dev/null ; then
        echo 'Found old container, deleting'
        docker container rm -f "$CONTAINER_ID"
    fi

    # The container image is specified by
    # the `CI_JOB_IMAGE` predefined variable
    # https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    # which is available under `CUSTOM_ENV_CI_JOB_IMAGE` to allow the
    # user to specify the image.
    docker run -d -t --name "$CONTAINER_ID" -v "$PROJECT_DIR:$PROJECT_DIR" -v "./$KUBECONF_NAME:/root/.kube/config" --network="kind" "$CUSTOM_ENV_CI_JOB_IMAGE" tail -f /dev/null

    # make sure docker run command was successful
    retVal=$?
    if [ $retVal -ne 0 ]; then
        echo 'docker run command exited with non-zero exit code: $retVal'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi
}

echo "Starting k8s-cluster..."
start_k8s_cluster

echo "Starting container $CONTAINER_ID..."
start_container

echo "Installing dependencies..."
install_dependencies
