#!/usr/bin/env bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

echo "Deleting cluster $CONTAINER_ID"
kind delete cluster --name "$CONTAINER_ID"

echo "Deleting container $CONTAINER_ID"
docker container rm -f "$CONTAINER_ID"

rm -f $KUBECONF_NAME
