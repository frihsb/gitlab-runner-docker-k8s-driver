#!/usr/bin/env bash
# k8s-kind-cluster-setup.sh <cluster_name> <mount_project_dir>
set -e  # exit script in case of error
export CLUSTER_NAME=$1
export MOUNT_PROJECT_DIR=$2
currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
tempfile=$(mktemp)
cat "$currentDir/k8s-kind-cluster-config.yaml" | envsubst > $tempfile
kind create cluster --config $tempfile --name $CLUSTER_NAME
rm $tempfile
