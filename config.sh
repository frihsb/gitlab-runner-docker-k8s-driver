#!/usr/bin/env bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base.

cat << EOS
{
  "builds_dir": "$BUILDS_DIR",
  "cache_dir": "$CACHE_DIR",
  "builds_dir_is_shared": false,
  "driver": {
    "name": "docker-k8s-driver",
    "version": "v1.0.0"
  }
}
EOS
